<?php
/**
 * NestedSet.php
 *
 * @author    Bernd Engels
 * @created   03.04.19 15:19
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Repositories;

use App\Models\Menu;
use Kalnoy\Nestedset\NodeTrait;

class MenuRepository {

	/**
	 * @var Menu
	 */
	protected $model;

	public function __construct() {
		$this->model = Menu::class;
	}

	public function getTree()
	{
		return Menu::get()->toTree();
	}

	public function getNode( $id, $withDepth = true)
	{
		return $withDepth ? Menu::withDepth()->find($id) : Menu::find($id);
	}

	public function getChildren( $id )
	{
		return $this->getNode($id, true)->children;
	}

	public function getPath( $id )
	{
		return $this->getNode($id)->depth;
	}

	public function create( $parentId )
	{

	}

	public function move( $id )
	{

	}

	public function copy( $id )
	{

	}

	public function rename( $id, $name )
	{

	}

	public function remove( $id = 1 )
	{

	}
}