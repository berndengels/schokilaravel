<?php
/**
 * UserController.php
 *
 * @author    Bernd Engels
 * @created   28.02.19 17:17
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Forms\PageForm;
use Illuminate\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class PageController extends MainController
{
    use FormBuilderTrait;

    static protected $model = Page::class;
    static protected $form = PageForm::class;

    public function store( Request $request, $id = 0 )
    {
        $form   = $this->form(PageForm::class);
        $entity = ($id > 0) ? Page::find($id) : new Page();

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $entity->title  = $form->title->getRawValue();
        $entity->slug   = Str::slug($form->title->getRawValue());
        $entity->body   = $form->body->getRawValue();
        $entity->is_published  = $form->is_published->getRawValue();

        try {
            $entity->saveOrFail();
            $id = $entity->id;
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        switch($request->submit) {
            case 'save':
                return redirect()->route('admin.pageEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.pageList');
        }
    }
}
