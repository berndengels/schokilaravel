<?php
/**
 * EventsController.php
 *
 * @author    Bernd Engels
 * @created   30.01.19 18:55
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\EventTemplate;
use App\Forms\EventTemplateForm;
use Illuminate\Support\Facades\DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class EventTemplateController extends MainController
{
    use FormBuilderTrait;

    static protected $model = EventTemplate::class;
    static protected $form	= EventTemplateForm::class;

	public function store( Request $request, $id = 0 ) {

        $form   = $this->form(EventTemplateForm::class);
        $event  = $id > 0 ? EventTemplate::find($id) : new EventTemplate();

        $event->title           = $form->title->getRawValue();
        $event->subtitle        = $form->subtitle->getRawValue();
        $event->description     = $form->description->getRawValue();
        $event->links           = $form->links->getRawValue();
        $event->category_id     = $form->category->getRawValue();
        $event->theme_id        = $form->theme->getRawValue();

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $event->saveOrFail();
        $id = $event->id;

        $this->processImages($request, $id);

        switch($request->submit) {
            case 'save':
                return redirect()->route('admin.eventTemplateEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.eventTemplateList');
        }
    }
}
