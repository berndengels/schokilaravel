<?php
/**
 * EventsController.php
 *
 * @author    Bernd Engels
 * @created   30.01.19 18:55
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use App\Forms\EventTemplateSelectForm;
use App\Forms\SearchForm;
use App\Models\EventPeriodic;
use App\Models\EventTemplate;
use Auth;
use App\Models\Theme;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\Event;
use App\Repositories\EventRepository;
use App\Repositories\EventPeriodicRepository;
use App\Models\Images;
use App\Forms\EventForm;
use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Http\Controllers\Admin\MainController;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class EventController extends MainController
{
    use FormBuilderTrait;

    static protected $model = Event::class;
    static protected $form	= EventForm::class;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listEvents(FormBuilder $formBuilder)
	{
		parent::index();
        $data = EventRepository::getEventsSinceToday()
			->sortable()
			->paginate($this->paginationLimit)
		;
        return view('admin.events', [
            'data'      => $data,
            'addLink'   => $this->addLink,
            'entity'    => $this->entity,
            'title'     => Str::plural($this->title),
			'templateSelectForm'	=> $formBuilder->create(EventTemplateSelectForm::class, ['url'	=> route('admin.eventTemplateSelect')]),
        ]);
    }

	public function edit( FormBuilder $formBuilder, $id = 0, $options = null ) {
		parent::initReservedDates();
		return parent::edit($formBuilder, $id, [
			'dates'		=> $this->strReservedDates,
		]);
	}

	public function archive( FormBuilder $formBuilder, Request $request )
	{
		$data = EventRepository::getEventsUntilDate($this->today, $request->get('search'))
			->sortable()
			->paginate($this->paginationLimit)
			->appends(Input::except('page'))
		;
		$form = $formBuilder->create(SearchForm::class, ['url'	=> route('admin.eventArchiveSearch')]);

		return view('admin.events', [
			'searchForm'	=> $form,
			'data'      	=> $data,
			'addLink'   	=> $this->addLink,
			'entity'    	=> $this->entity,
			'title'     	=> 'Archiv',
			'templateSelectForm'=> null,
		]);
	}

	public function checkForPeriodicDate( Request $request ) {
		$date		= $request->post('date');
		$newDate	= Carbon::createFromTimeString($date, 'Europe/Berlin');
		$repo		= new EventPeriodicRepository();
		$entity		= $repo->getPeriodicEventByDate($newDate->format('Y-m-d'));
		$response	= [
			'date'		=> $newDate->format('d.m.Y'),
			'entity'	=> ($entity) ? $entity->toArray() : null,
		];

		return response()->json($response);
	}

	public function override( FormBuilder $formBuilder, Request $request, $id = 0 )
	{
		parent::initReservedDates();
		$eventPeriodicId = $request->post('override');
		$eventPeriodic = EventPeriodic::find($eventPeriodicId);
		$event = $id > 0 ? Event::find($id) : new Event();
		$override = $request->post('override');

		$attributes = [
			'is_periodic' 	=> true,
			'title'			=> $eventPeriodic->title,
			'subtitle'		=> $eventPeriodic->subtitle,
			'description'	=> $eventPeriodic->description,
			'links'			=> $eventPeriodic->links,
			'category'		=> $eventPeriodic->category,
			'theme'			=> $eventPeriodic->theme,
			'images'		=> $eventPeriodic->images,
			'event_time'	=> $eventPeriodic->event_time,
			'event_date'	=> $request->post('event_date'),
			'override'		=> $override,
		];

		$event->setRawAttributes($attributes);

		$form  = $formBuilder->create(EventForm::class, ['model' => $event]);
		$formOptions    = [
			'id'        => $id,
			'form'      => $form,
			'title'     => $this->title,
			'listLink'  => $this->listLink,
			'dates' 	=> $this->strReservedDates,
		];
		return view('admin.form.'.$this->entity, $formOptions);
	}

	public function store( Request $request, $id = 0 ) {

        $form   = $this->form(EventForm::class);
        $event  = $id > 0 ? Event::find($id) : new Event();
		$override = $request->post('override');
		$template = $request->post('template');

        $event->title           = $form->title->getRawValue();
        $event->subtitle        = $form->subtitle->getRawValue();
        $event->event_time      = '' !== $form->event_time->getRawValue() ? $form->event_time->getRawValue() : config('event.defaultEventTime');
        $event->event_date      = $form->event_date->getRawValue();
        $event->description     = $form->description->getRawValue();
        $event->is_published    = isset($form->is_published) ? 1 : 0;
		$event->is_periodic     = isset($form->is_periodic) ? 1 : 0;
        $event->links           = $form->links->getRawValue();
        $event->category_id     = $form->category->getRawValue();
        $event->theme_id        = $form->theme->getRawValue();

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $event->saveOrFail();
        $id = $event->id;

        $this->processImages($request, $id, $override, $template);

//		exec('npm run dev');
//		\Artisan::call('view:clear');

		switch($request->submit) {
            case 'save':
                return redirect()->route('admin.eventEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.eventList');
        }
    }

	public function template(FormBuilder $formBuilder, Request $request ) {
			$id = $request->post('eventTemplate');

			if(!$id) {
				return null;
			}

			$template  = EventTemplate::find($id);
			if($template) {
				parent::initReservedDates();
				$event = new Event();

				$attributes = [
					'is_periodic' 	=> true,
					'title'			=> $template->title,
					'subtitle'		=> $template->subtitle,
					'description'	=> $template->description,
					'links'			=> $template->links,
					'category'		=> $template->category,
					'theme'			=> $template->theme,
					'images'		=> $template->images,
					'event_time'	=> $template->event_time,
					'template'		=> $id,
				];

				$event->setRawAttributes($attributes);

				$form  = $formBuilder->create(EventForm::class, ['model' => $event]);
				$data    = [
					'id'        => null,
					'form'      => $form,
					'title'     => $this->title,
					'listLink'  => $this->listLink,
					'dates' 	=> $this->strReservedDates,
				];
				return view('admin.form.'.$this->entity, $data);

			}
	}
}
