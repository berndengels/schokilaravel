<?php

namespace App\Http\Controllers;

use Egulias\EmailValidator\Exception\ExpectingDomainLiteralClose;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class PageController
 */
class StaticPageController extends BaseController
{
    use ValidatesRequests;

	/**
	 * @param $page
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function get() {
		$path	= app()->basePath() . '/resources/views/public/static';
		$route	= Route::currentRouteName();
		$page	= collect(explode('.',$route))->last();
		$file	= "$path/$page.blade.php";
		if( file_exists($file) ) {
	        return view('public.static.' . $page, ['data' => null ]);
		}
		return null;
    }
}
