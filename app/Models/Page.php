<?php

namespace App\Models;

use App\Models\Ext\HasUser;
use App\Models\Image;
use Illuminate\Support\Str;
use App\Models\Ext\Userable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 */
class Page extends Model
{
	use Sortable, HasUser;

	public $sortable = [
		'title',
		'created_at',
	];
	/**
     * @var string
     */
    protected $table = 'page';
    /**
     * @var array
     */
    protected $fillable = ['title', 'body', 'created_by', 'updated_by'];
	protected $dates = ['created_at','updated_at'];
	public $bodySanitized;

    public static function boot() {
        parent::boot();

		Page::saving(function($entity) {
			$entity->slug = Str::slug( str_replace('.','-',$entity->title), '-', 'de');
		});
		Page::creating(function($entity) {
			$entity->slug = Str::slug(str_replace('.','-',$entity->title), '-', 'de');
		});
		Page::updating(function($entity) {
			$entity->slug = Str::slug(str_replace('.','-',$entity->title), '-', 'de');
		});

		Page::retrieved(function($entity) {
			$wrapper = '<div class="row embed-responsive-wrapper text-center"><div class="embed-responsive embed-responsive-16by9 m-0 p-0">%%</div></div>';
			$entity->bodySanitized = preg_replace("/(<iframe[^>]+><\/iframe>)/i", str_replace('%%','$1', $wrapper), $entity->body);
		});
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function audios()
	{
		return $this->hasMany('App\Models\Audios');
	}

	/**
	 * @param string $value
	 * @return array
	 */
	public function getBodyAttribute($value = '')
	{
		if('' !== $value) {
			return preg_replace(['/^<p>(<br[ ]?[\/]?>){1,}/i','/(<br[ ]?[\/]?>){1,}<\/p>$/i'],['<p>','</p>'], trim($value));
		}
	}
}
