<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Address
 */
class Address extends Model
{
	use Sortable;

	protected $table = 'address';
    protected $fillable = ['address_category_id','email','token'];
    public $timestamps = true;
	public $sortable = [
		'addressCategory',
		'email',
		'created_at',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function addressCategory()
	{
		return $this->belongsTo(AddressCategory::class, 'address_category_id', 'id');
	}
}
