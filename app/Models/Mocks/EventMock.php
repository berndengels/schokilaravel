<?php
/**
 * EventMock.php
 *
 * @author    Bernd Engels
 * @created   07.05.19 17:22
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Models\Mocks;

use Illuminate\Database\Eloquent\Model;

class EventMock extends Model {

	protected $table = 'events_mock';
	protected $fillable = ['title','description'];
	public $timestamps = true;

}