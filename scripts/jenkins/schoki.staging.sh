#!/usr/bin/env bash

PATH="/home/schoki/local/bin:/bin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin"
dir=$(pwd)
pagePath="$dir/web/schokoladen-mitte.de/test.schokoladen-mitte.de"
user=$(whoami)
composer=./composer
npm="/home/schoki/local/bin/npm"
echo "path: $PATH"
echo "user: $user"
cd $pagePath
echo "dir: $(pwd)"

git fetch origin
git pull

if ${COPY_SCRIPTS}; then
    cp -f ./scripts/public-htaccess.tpl ./public_html/.htaccess
    cp -f ./scripts/phpunit.dusk.xml.schoki phpunit.dusk.xml
    cp -f .env.staging .env
fi

if ${COMPOSER_INSTALL}; then
    echo "composer install ;-)"
    php $composer install
fi
if ${COMPOSER_UPDATE}; then
    echo "composer update ;-)"
    php $composer update
fi
if ${NPM_INSTALL}; then
    echo "npm install ;-)"
    $npm cache verify
    $npm install
fi
if ${CLEAR}; then
    php artisan cache:clear
    php artisan config:clear
    php artisan route:clear
    php artisan view:clear
    $npm run dev
fi
if ${RUN_TEST}; then
   echo "run tests ;-)"
   php artisan dusk --debug --verbose
fi
