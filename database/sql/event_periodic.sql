ALTER TABLE `event_periodic` ADD `event_date` DATE NULL AFTER `start_date`;
ALTER TABLE `event_periodic` DROP `start_date`;
ALTER TABLE `event_periodic` DROP `is_periodic`;

UPDATE `periodic_position` SET `search_key` = 'first' WHERE `id` = '2';
UPDATE `periodic_position` SET `search_key` = 'second' WHERE `id` = '3';
UPDATE `periodic_position` SET `search_key` = 'third' WHERE `id` = '4';
UPDATE `periodic_position` SET `search_key` = 'last' WHERE `id` = '5';
UPDATE `periodic_position` SET `search_key` = '2' WHERE `id` = '6';
UPDATE `periodic_position` SET `search_key` = '3' WHERE `id` = '7';
UPDATE `periodic_position` SET `search_key` = '4' WHERE `id` = '8';
UPDATE `periodic_position` SET `search_key` = '1' WHERE `id` = '1';
