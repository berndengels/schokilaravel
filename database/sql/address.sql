ALTER TABLE `address` DROP `fon`;
ALTER TABLE `address` CHANGE `salt` `token` VARCHAR(50)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';
ALTER TABLE `address` ADD `info_on_changes` TINYINT(1)  UNSIGNED  NULL  DEFAULT NULL  AFTER `token`;
ALTER TABLE `address` DROP `name`;
ALTER TABLE `address` CHANGE `address_category_id` `address_category_id` INT(11)  UNSIGNED  NOT NULL;
ALTER TABLE `address` CHANGE `id` `id` INT(11)  UNSIGNED  NOT NULL  AUTO_INCREMENT;

DELETE FROM `address` WHERE `address_category_id` NOT IN(4,5);
DELETE FROM `address_category` WHERE `id` IN ('1','2','3');

-- ALTER TABLE `address` ADD CONSTRAINT `fkAddressCategory` FOREIGN KEY (`address_category_id`) REFERENCES `address_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `message_subject` DROP `slug`;
ALTER TABLE `message_subject` CHANGE `subject` `name` VARCHAR(50)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';
ALTER TABLE `message_subject` CHANGE `id` `id` INT(11)  UNSIGNED  NOT NULL  AUTO_INCREMENT;
ALTER TABLE `address_category` DROP `slug`;
ALTER TABLE `address_category` CHANGE `id` `id` INT(11)  UNSIGNED  NOT NULL  AUTO_INCREMENT;
ALTER TABLE `address_category` CHANGE `id` `id` INT(11)  UNSIGNED  NOT NULL  AUTO_INCREMENT;
ALTER TABLE `address_category` DROP `is_public`;

UPDATE `address_category` SET `id` = '1' WHERE `id` = '4';
UPDATE `address_category` SET `id` = '2' WHERE `id` = '5';
ALTER TABLE `address_category` AUTO_INCREMENT = 1;

UPDATE `address` SET `address_category_id` = '1' WHERE `address_category_id` = '4';
UPDATE `address` SET `address_category_id` = '2' WHERE `address_category_id` = '5';
