-- event
DROP TABLE `event_periodic_mapper`;

-- category
ALTER TABLE `category` CHANGE `is_published` `is_published` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0';
ALTER TABLE `category` CHANGE `name` `name` VARCHAR(50)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '0';
ALTER TABLE `category` CHANGE `default_time` `default_time` VARCHAR(10)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT '';
ALTER TABLE `category` CHANGE `default_price` `default_price` INT(11)  NULL;
ALTER TABLE `category` ADD `icon` VARCHAR(50)  NULL  DEFAULT NULL  AFTER `slug`;
ALTER TABLE `category` DROP `num_events`;

TRUNCATE TABLE `category`;
INSERT INTO `category` (`id`, `name`, `slug`, `icon`, `default_time`, `default_price`, `is_published`)
VALUES
  (1, 'Musik', 'musik', 'md-musical-notes', '19.00', 0, 1),
  (2, 'DJ', 'dj', 'md-headset', '22.00', 0, 1),
  (3, 'Lesung', 'lesung', 'md-book', '20.30', 0, 1),
  (4, 'Film', 'film', 'md-film', '21.00', 0, 1),
  (5, 'Ausstellung', 'ausstellung', 'md-eye', '', 0, 1),
  (6, 'Party', 'party', 'md-happy', '21.00', 0, 1),
  (7, 'Spezial', 'spezial', 'md-star', '', 0, 1),
  (8, 'Kneipe', 'kneipe', 'md-beer', '19.00', 0, 1);

-- theme
ALTER TABLE `theme` CHANGE `is_published` `is_published` TINYINT(1)  NOT NULL  DEFAULT '0';
ALTER TABLE `theme` CHANGE `id` `id` INT(11)  UNSIGNED  NOT NULL  AUTO_INCREMENT;

SELECT `email` FROM `my_user`
WHERE exists(
  SELECT *
  FROM `music_style`
  INNER JOIN `user_music_styles` ON `music_style`.`id` = `user_music_styles`.`music_style_id`
  WHERE `my_user`.`id` = `user_music_styles`.`user_id` AND `musicStyles`.`id` = 7
);

UPDATE event SET event_time = REPLACE(event_time, '.', ':') WHERE event_time LIKE '%.%';
UPDATE event_periodic SET event_time = REPLACE(event_time, '.', ':') WHERE event_time LIKE '%.%';
