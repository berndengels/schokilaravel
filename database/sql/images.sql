SELECT
	e.title,
	u.`email`,
	e.`event_date`,
	m.`internal_filename`,
	m.`external_filename`,
	i.`width`,
	i.`height`,
	COUNT(width) anzahl
FROM `image` i
JOIN media m ON m.id=i.id
JOIN EVENT e ON e.id=m.`event_id`
JOIN `my_user` u ON u.id = e.`created_by`
WHERE `height` > '0'
GROUP BY width,height
ORDER BY anzahl DESC,height LIMIT 1000;
