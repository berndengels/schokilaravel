-- image
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `event_periodic_id` int(11) DEFAULT NULL,
  `external_filename` varchar(100) NOT NULL,
  `internal_filename` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `extension` varchar(10) NOT NULL,
  `filesize` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `eventPeriodic_id` int(11) DEFAULT NULL,
  `width` smallint(5) unsigned DEFAULT NULL,
  `height` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_image_event` (`event_id`),
  KEY `idx_image_page` (`page_id`),
  KEY `idx_image_event_periodic` (`eventPeriodic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `images`
(
  `id`,
  `event_id`,
  `page_id`,
  `event_periodic_id`,
  `external_filename`,
  `internal_filename`,
  `title`,
  `extension`,
  `filesize`,
  `updated_by`,
  `created_by`,
  `created_at`,
  `updated_at`,
  `eventPeriodic_id`,
  `width`,
  `height`
)
  SELECT
    i.`id`,
    m.`event_id`,
    m.`page_id`,
    m.`event_periodic_id`,
    m.`external_filename`,
    m.`internal_filename`,
    m.`title`,
    m.`extension`,
    m.`filesize`,
    m.`updated_by`,
    m.`created_by`,
    m.`created_at`,
    m.`updated_at`,
    m.`eventPeriodic_id`,
    i.`width`,
    i.`height`
  FROM media m
    JOIN image i ON i.id = m.id
  WHERE file_type = 1;

-- audio
DROP TABLE IF EXISTS `audios`;
CREATE TABLE `audios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `event_periodic_id` int(11) DEFAULT NULL,
  `external_filename` varchar(100) NOT NULL,
  `internal_filename` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `extension` varchar(10) NOT NULL,
  `filesize` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `eventPeriodic_id` int(11) DEFAULT NULL,
  `duration` int(11) unsigned NOT NULL,
  `bitrate` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_audio_event` (`event_id`),
  KEY `idx_audio_page` (`page_id`),
  KEY `idx_audio_event_periodic` (`eventPeriodic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `audios`
(
  `id`,
  `event_id`,
  `page_id`,
  `event_periodic_id`,
  `external_filename`,
  `internal_filename`,
  `title`,
  `extension`,
  `filesize`,
  `updated_by`,
  `created_by`,
  `created_at`,
  `updated_at`,
  `eventPeriodic_id`,
  `duration`,
  `bitrate`
)
SELECT
  a.`id`,
  m.`event_id`,
  m.`page_id`,
  m.`event_periodic_id`,
  m.`external_filename`,
  m.`internal_filename`,
  m.`title`,
  m.`extension`,
  m.`filesize`,
  m.`updated_by`,
  m.`created_by`,
  m.`created_at`,
  m.`updated_at`,
  m.`eventPeriodic_id`,
  a.`duration`,
  a.`bitrate`
FROM media m
JOIN audio a ON a.id = m.id
WHERE file_type = 2;

-- video
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `event_periodic_id` int(11) DEFAULT NULL,
  `external_filename` varchar(100) NOT NULL,
  `internal_filename` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `extension` varchar(10) NOT NULL,
  `filesize` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `eventPeriodic_id` int(11) DEFAULT NULL,
  `width` smallint(5) unsigned DEFAULT NULL,
  `height` smallint(5) unsigned DEFAULT NULL,
  `duration` int(11) unsigned NOT NULL,
  `bitrate` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_video_event` (`event_id`),
  KEY `idx_video_page` (`page_id`),
  KEY `idx_video_event_periodic` (`eventPeriodic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `videos`
(
  `id`,
  `event_id`,
  `page_id`,
  `event_periodic_id`,
  `external_filename`,
  `internal_filename`,
  `title`,
  `extension`,
  `filesize`,
  `updated_by`,
  `created_by`,
  `created_at`,
  `updated_at`,
  `eventPeriodic_id`,
  `width`,
  `height`,
  `duration`,
  `bitrate`
)
SELECT
  v.`id`,
  m.`event_id`,
  m.`page_id`,
  m.`event_periodic_id`,
  m.`external_filename`,
  m.`internal_filename`,
  m.`title`,
  m.`extension`,
  m.`filesize`,
  m.`updated_by`,
  m.`created_by`,
  m.`created_at`,
  m.`updated_at`,
  m.`eventPeriodic_id`,
  v.`width`,
  v.`height`,
  v.`duration`,
  v.`bitrate`
FROM media m
JOIN videos v ON v.id = m.id
WHERE file_type = 3;

-- media
DROP TABLE IF EXISTS `media`;
