
ALTER TABLE `my_user` DROP `username_canonical`;
ALTER TABLE `my_user` DROP `email_canonical`;
ALTER TABLE `my_user` DROP `locked`;
ALTER TABLE `my_user` DROP `expired`;
ALTER TABLE `my_user` DROP `expires_at`;
ALTER TABLE `my_user` DROP `confirmation_token`;
ALTER TABLE `my_user` DROP `password_requested_at`;
ALTER TABLE `my_user` DROP `roles`;
ALTER TABLE `my_user` DROP `credentials_expired`;
ALTER TABLE `my_user` DROP `credentials_expire_at`;
ALTER TABLE `my_user` DROP `date_of_birth`;
ALTER TABLE `my_user` DROP `firstname`;
ALTER TABLE `my_user` DROP `lastname`;
ALTER TABLE `my_user` DROP `website`;
ALTER TABLE `my_user` DROP `biography`;
ALTER TABLE `my_user` DROP `gender`;
ALTER TABLE `my_user` DROP `locale`;
ALTER TABLE `my_user` DROP `timezone`;
ALTER TABLE `my_user` DROP `phone`;
ALTER TABLE `my_user` DROP `facebook_uid`;
ALTER TABLE `my_user` DROP `facebook_name`;
ALTER TABLE `my_user` DROP `facebook_data`;
ALTER TABLE `my_user` DROP `twitter_uid`;
ALTER TABLE `my_user` DROP `twitter_name`;
ALTER TABLE `my_user` DROP `twitter_data`;
ALTER TABLE `my_user` DROP `gplus_uid`;
ALTER TABLE `my_user` DROP `gplus_name`;
ALTER TABLE `my_user` DROP `gplus_data`;
ALTER TABLE `my_user` DROP `two_step_code`;
ALTER TABLE `my_user` DROP `group_id`;
ALTER TABLE `my_user` DROP `token`;
ALTER TABLE `my_user` DROP `salt`;

ALTER TABLE `my_user` ADD `remember_token` VARCHAR(100)  NULL  DEFAULT NULL AFTER `last_login`;
ALTER TABLE `my_user` CHANGE `enabled` `enabled` TINYINT(1)  NULL  DEFAULT '0';
DELETE FROM `my_user` WHERE `last_login` IS NULL;
ALTER TABLE `my_user` ADD `is_super_admin` TINYINT  UNSIGNED  NULL  DEFAULT NULL  AFTER `id`;

RENAME TABLE `bookers_music_styles` TO `user_music_styles`;
ALTER TABLE `user_music_styles` CHANGE `booker_id` `user_id` INT(11) UNSIGNED NOT NULL;
ALTER TABLE `user_music_styles` CHANGE `musicstyle_id` `music_style_id` INT(11) NOT NULL;
ALTER TABLE `user_music_styles` CHANGE `music_style_id` `music_style_id` INT(11) UNSIGNED NOT NULL;

CREATE VIEW `view_user_music_styles`
AS SELECT
     `u`.`username` AS `USER`,
     `m`.`name` AS `musicStyle`
   FROM ((`user_music_styles` `s` join `my_user` `u` on((`u`.`id` = `s`.`user_id`))) join `music_style` `m` on((`m`.`id` = `s`.`music_style_id`))) order by `u`.`username`;