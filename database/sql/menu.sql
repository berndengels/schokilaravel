ALTER TABLE `menu` DROP `page_id`;
ALTER TABLE `menu` DROP `root`;
ALTER TABLE `menu` DROP `sort_order`;
ALTER TABLE `menu` DROP `slug`;
ALTER TABLE `menu` ADD `icon` VARCHAR(100)  NULL  DEFAULT NULL  AFTER `name`;

TRUNCATE TABLE `menu_item_type`;
INSERT INTO `menu_item_type` (`id`, `type`, `label`)
VALUES
	(1, 'label', 'Label'),
	(2, 'link', 'externer Link'),
	(3, 'route', 'interne Route'),
	(4, 'topMenuRoot', 'Top Menu Root'),
	(5, 'bottomMenuRoot', 'Bottom Menu Root');
