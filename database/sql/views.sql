CREATE VIEW `view_user_music_styles`
AS SELECT
   `u`.`username` AS `USER`,
   `m`.`name` AS `musicStyle`
FROM ((`user_music_styles` `s` join `my_user` `u` on((`u`.`id` = `s`.`user_id`))) join `music_style` `m` on((`m`.`id` = `s`.`music_style_id`))) order by `u`.`username`;
