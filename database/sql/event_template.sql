CREATE TABLE `event_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `theme_id` int(11) unsigned DEFAULT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `description` longtext,
  `links` longtext,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3BAE0AA759027487` (`theme_id`),
  KEY `IDX_3BAE0AA712469DE2` (`category_id`),
  KEY `IDX_3BAE0AA7DE12AB56` (`created_by`),
  KEY `IDX_3BAE0AA716FE72E1` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `images` ADD `event_template_id` INT(11) unsigned  NULL  DEFAULT NULL  AFTER `event_periodic_id`;
